from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Contenido

# Create your views here.


@csrf_exempt
def get_content(request, clave):
    try:
        contenido = Contenido.objects.get(clave=clave)
        if request.method == "GET":

            return HttpResponse(contenido.valor)
        elif request.method == "PUT":

            return HttpResponse("already in database  " + clave)
    except Contenido.DoesNotExist:

        if request.method == "PUT":
            valor = request.body.decode("utf-8")
            new_contenido = Contenido(clave=clave, valor=valor)
            new_contenido.save()
        else:
            return HttpResponse("Error norequest method " + clave)
